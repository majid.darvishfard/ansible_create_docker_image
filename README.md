### Description
-------------
##### this playbook get app version and download last app source from gitlab repo and  create a Docker Image Base on the app version and push that to Docker hub rejistry .
#####  By default the created image has the same name as the app:v1.0.0 format.




### Before Run playbook:
---------
#####  before run this playbook make this steps:
#####  1- run  `ansible-vault encrypt_string '**YOUR_DOCKER_registery_PASSWORD**' --name 'docker_registry_password'`  for create new vault variable for use in playbook. 
#####  2- edit playbook variable :
    app_name: app   
    app_version: v4.0.0
    docker_registry_username: XXXXXXX
    docker_registry_password: !vault |
          $ANSIBLE_VAULT;1.1;AES256
          33633035653266363466353738303735616264666534323532336531623634326665653464346437
          3164353437343331343135326630396162393363616261340a633230616239666130333530393866
          38373731356662393465323235366538356638666434356532383436623261373633373130303433
          6166313864626361380a383538353933653961633666336232326535326136393739313138393134
          6162

    docker_registry_email: XXXXXXXXXX
 


### How to run the playbook:
------------------
##### for run playbook use this command `ansible-playbook playbook.yml --ask-vault-pass` 


### requarment
 this playbook need  docker. use this [playbook](https://gitlab.com/majid.darvishfard/ansible_os_config) for install docker. 
